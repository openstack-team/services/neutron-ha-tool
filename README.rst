Additional command line utility for OpenStack Neutron HA operations

Neutron HA tool allows one to manipulate the different components of
OpenStack Neutron. For example, it makes it possible to delete, list or
migrate all the agents that Neutron provides. This way, it is possible
to delete, list or migrate virtual routers in a full HA way.
